<html>
	<body>
		<h1> Buat Account Baru </h1>
		<h2> Sign Up Form </h2>
		<!-- form text nama-->
		<form action="/welcome" method="POST">
			@csrf
		  <label for="fname"> First Name: </label><br><br>
		  <input type = "text" id="fname" name= "fname"><br><br>
		  <label for="lname"> Last Name: </label><br><br>
		  <input type = "text" id="lname" name= "lname"><br><br>

		<!-- form radio jenis kelamin-->

		  <label> Gender: </label><br><br>
		  <input type = "radio" id="Male" name= "gender" value = "Male">
		  <label for="Male"> Male </label><br>
 		  <input type = "radio" id="Female" name= "gender" value="Female">
		  <label for="Female"> Female </label><br>
		  <input type = "radio" id="Other" name= "gender" value="Other">
		  <label for="Other"> Other </label><br><br>

		<!-- form list nationalitas-->

		  <label> Nationality: </label><br><br>
		  <select>
		    <option> Indonesia </option>
		    <option> Japan </option>
		    <option> America </option>
		    <option> United Kingdon </option>
		    <option> Singapore </option>
		    <option> India </option>
		    <option> Russia </option>
		    <option> South Korea </option>
		    <option> Other </option>
		  </select>
		  <br><br>

		<!-- form checkbox Bahasa-->

		  <label> Languange Spoken </label><br><br>
		  <input type="checkbox" id="Bahasa Indonesia" name = "languange" value = "Bahasa Indonesia">
		  <label for = "Bahasa Indonesia"> Bahasa Indonesia </label><br>
		  <input type="checkbox" id="English" name = "languange" value = "English">
		  <label for = "English"> English </label><br>
		  <input type="checkbox" id="Other" name = "languange" value = "Other">
		  <label for="Other"> Other </label>

		<!-- form textarea biodata-->

		  <label> Bio: </label><br><br>
		  <textarea cols ="30" rows="8" text-align = "justify">
		  </textarea><br><br>
		  <input type = 'submit' value = 'Sign Up'>
		</form>

	</body>
</html>
